import {Card, Button} from 'react-bootstrap';
export default function CourseCard(){
	return(

        <Card className="cardHighlight p-2">
            <Card.Body>
                <Card.Title>
                    <h2>Sample Course</h2>
                </Card.Title>

                <Card.Text>
                    Description:
                </Card.Text>
                <Card.Text>
                    This is a sample course offering
                </Card.Text>
                <Card.Text>
                    Price:
                </Card.Text>
                <Card.Text>
                    Php 40,000
                </Card.Text>
                <Button variant="primary">Enroll</Button>
                


            </Card.Body>
        </Card>

		)
}