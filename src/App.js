
import './App.css';
import AppNavbar from './components/AppNavbar.js'
import Home from './pages/Home.js';
import {Container} from 'react-bootstrap/';


function App() {
  return (
    //we are mounting our components to prepare for output for rendering
    //para di magagawan ng space ang components kailangan ng parent
    //Fragment "<> and </> to act as parents of components"
    <>
      <AppNavbar/>
      <Container>
        <Home/>
      </Container>


    </>
  );
}

export default App;
